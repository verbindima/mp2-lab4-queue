// ����, ���, ���� "������ ����������������-2", �++, ���
//
// tdataroot.h - Copyright (c) ������� �.�. 28.07.2000 (06.08)
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (21.04.2015)
//
// ������������ ��������� ������ - ������� (�����������) ����� - ������ 3.2
//   ������ ���������� ����������� ��� �������� ������� SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "TDataCom.h"

#define DefMemSize   25  // ������ ������ �� ���������

#define DataEmpty  -101  // �� �����
#define DataFull   -102  // �� �����������
#define DataNoMem  -103  // ��� ������

typedef int    TElem;    // ��� �������� ��
typedef TElem* PTElem;
typedef int    TData;    // ��� �������� � ��

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot: public TDataCom {
protected:
  PTElem pMem;      // ������ ��� ��
  int memSize;      // ������ ������ ��� ��
  int dataCount;    // ���������� ��������� � ��
  TMemType memType; // ����� ���������� �������
  void setMem(void* p, int Size);   // ������� ������

public:
  virtual ~TDataRoot();
  TDataRoot(int size = DefMemSize);
  virtual bool isEmpty(void) const;           // �������� ������� ��
  virtual bool isFull (void) const;           // �������� ������������ ��
  virtual void  put   (const TData &Val) = 0; // �������� ��������
  virtual TData get   (void)             = 0; // ������� ��������

  // ��������� ������
  //virtual int  IsValid() = 0;                 // ������������ ���������
  virtual void print()   = 0;                 // ������ ��������

  // ������������� ������
  /*friend class TMultiStack;
  friend class TSuperMultiStack;
  friend class TComplexMultiStack;*/
};

#endif