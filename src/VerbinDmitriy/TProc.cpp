#ifndef __TPROC_CPP__
#define __TPROC_CPP__

#include <cstdlib>
#include "TProc.h"

TProc::TProc(char performance) : q2(performance), jobDuration(JOB_DURATION_DISABLED), isWorkingStatus(false) {

    if (performance < 0 || performance > MAX_CPU_PERFORMANCE) {
        throw 1;
    }

}

void TProc::cycle() {

    if (jobDuration == JOB_DURATION_DISABLED && (rand() % MAX_CPU_PERFORMANCE < q2)
        || !--jobDuration) {
        isWorkingStatus = false;
    }

}

void TProc::startNewJob(int duration) {

    if (duration < 0 && duration != JOB_DURATION_DISABLED) {
        throw 2;
    }

    isWorkingStatus = true;

}

bool TProc::isWorking() {

    return isWorkingStatus;

}

#endif