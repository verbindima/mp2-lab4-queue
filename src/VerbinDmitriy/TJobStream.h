#ifndef __TJOBSTREAM_H__
#define __TJOBSTREAM_H__

#include "TQueue.h"
#include "TProc.h"
#define MAX_FLOW_INTENSITY 100
#define DEFAULT_CPU_CYCLES 1000000

class TJobStream {
private:
    TProc CPU;
    TQueue jobsQueue;
    int q1; // "������������� ������ �������", ����������� [0; MAX_FLOW_INTENSITY]
    int currID; // ���������� ����� �������
    int stats_missed, stats_cycles, stats_useless_cycles; // �������������� ����������
    void cycle();

public:
    // � jobsIntensity � performance �� ��������� ����. �������� 100 
    TJobStream(char jobsIntensity, char performance, int queueLen);
    void startJobStream(int cycles = DEFAULT_CPU_CYCLES);
    void showStats();
};

#endif