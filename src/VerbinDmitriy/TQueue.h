#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

class TQueue: public TStack {
protected:
    int lowIndex;
    int getNextIndex(int index);

public:
    TQueue(int size = DefMemSize);
    virtual TData get() override;
    virtual void print() override;
};

#endif