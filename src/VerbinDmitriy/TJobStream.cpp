#ifndef __TJOBSTREAM_CPP__
#define __TJOBSTREAM_CPP__

#include <cstdlib>
#include <iostream>
#include "TJobStream.h"

TJobStream::TJobStream(char jobsIntensity, char performance, int queueLen):
    q1(jobsIntensity), CPU(performance), jobsQueue(queueLen) {

    if (jobsIntensity < 0 || jobsIntensity > MAX_FLOW_INTENSITY) {
        throw 1;
    }

    currID = stats_missed = stats_cycles = stats_useless_cycles = 0;

}

void TJobStream::cycle() {

    if (CPU.isWorking()) {
        ++stats_cycles;
    } else {
        ++stats_useless_cycles;
    }

    if (!CPU.isWorking() && !jobsQueue.isEmpty()) {
        jobsQueue.get();
        CPU.startNewJob();
    }

    CPU.cycle();

}

void TJobStream::startJobStream(int cycles) {

    for (int i = 0; i < cycles; i++) {

        if (rand() % MAX_FLOW_INTENSITY < q1) {
            ++currID;

            if (jobsQueue.isFull()) {
                ++stats_missed;
            } else {
                jobsQueue.put(currID);
            }
        }

        cycle();

    }

    while (!jobsQueue.isEmpty()) {
        cycle();
    }

}

void TJobStream::showStats() {

    std::cout << "Cycles amount: " << (stats_cycles + stats_useless_cycles) << std::endl;
    std::cout << "Useless cycles: " << stats_useless_cycles << " (" <<
        (double)stats_useless_cycles / (double)(stats_cycles + stats_useless_cycles) * 100 << "%)" << std::endl;
    std::cout << "Jobs: " << currID << std::endl;
    std::cout << "Missed jobs: " << stats_missed << " (" <<
        (double)stats_missed / (double)(currID) * 100 << "%)" << std::endl;
    std::cout << "Cycles per job: " << (double)stats_cycles / (double)(currID - stats_missed) << std::endl;

}

#endif