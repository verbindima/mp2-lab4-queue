#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "TDataRoot.h"

class TStack: public TDataRoot {
protected:
    int highIndex;
    int getNextIndex(int index);

public:
    TStack(int size = DefMemSize);
    virtual void put(const TData&) override;
    virtual TData get() override;
    virtual void print() override;
};

#endif