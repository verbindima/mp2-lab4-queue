# Отчёт по выполненной работе "Структура данных Очередь"
## Введение

**Цель данной работы** — практическое освоение динамической структуры данных **Очередь**. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

Для начала вспомним определение очереди:

**Очередь** (англ. queue) – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым. 

## Разработка простой вычислительной системы на основе очереди
Очередь нами будет использована в первую очередь для реализации простой вычислительной системы с одним процессором. Опишем далее подробности того, что будет представлять из себя данная ВС.

Для ВС с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. При построении модели функционирования ВС должны учитываться следующие основные моменты обслуживания заданий:

- генерация нового задания;
- постановка задания в очередь для ожидания момента освобождения процессора;
- выборка задания из очереди при освобождении процессора после обслуживания очередного задания.

По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента (интенсивность потока заданий, размер очереди заданий, производительность процессора, число тактов имитации) и полученные в результате имитации показатели функционирования вычислительной системы, в т.ч.

- количество поступивших в ВС заданий;
- количество отказов в обслуживании заданий из-за переполнения очереди;
- среднее количество тактов выполнения заданий;
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.

Показатели функционирования вычислительной системы, получаемые при помощи систем имитации, могут использоваться для оценки эффективности применения ВС; по результатам анализа показателей могут быть приняты рекомендации о целесообразной модернизации характеристик ВС (например, при длительных простоях процессора и при отсутствии отказов от обслуживания заданий желательно повышение интенсивности потока обслуживаемых заданий и т.д.).

## Реализация базовых классов
Здесь мы используем уже известную нам иеарархию классов TDataCom -> TDataRoot -> TStack (последний класс со стеком был немного модифицирован с целью реализации наследования от него).

### Класс TDataCom
```C++
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

// TDataCom является общим базовым классом
class TDataCom {
protected:
  int RetCode; // Код завершения
  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}

  int GetRetCode() {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```

### Класс TDataRoot, заголовок и реализация
```C++
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "TDataCom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

typedef int    TElem;    // тип элемента СД
typedef TElem* PTElem;
typedef int    TData;    // тип значений в СД

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot: public TDataCom {
protected:
  PTElem pMem;      // память для СД
  int memSize;      // размер памяти для СД
  int dataCount;    // количество элементов в СД
  TMemType memType; // режим управления памятью
  void setMem(void* p, int Size);	// задание памяти

public:
  virtual ~TDataRoot();
  TDataRoot(int size = DefMemSize);
  virtual bool isEmpty(void) const;           // контроль пустоты СД
  virtual bool isFull (void) const;           // контроль переполнения СД
  virtual void  put   (const TData &Val) = 0; // добавить значение
  virtual TData get   (void)             = 0; // извлечь значение

  // служебные методы
  //virtual int  IsValid() = 0;                 // тестирование структуры
  virtual void print()   = 0;                 // печать значений

  // дружественные классы
  /*friend class TMultiStack;
  friend class TSuperMultiStack;
  friend class TComplexMultiStack;*/
};

#endif

```

```C++
#ifndef __TDATAROOT_CPP__
#define __TDATAROOT_CPP__

#include "TDataRoot.h"

TDataRoot::TDataRoot(int size): TDataCom() {

	memSize = size;
	dataCount = 0;

	if (size == 0) {
		memType = MEM_RENTER;
	} else {
		memType = MEM_HOLDER;
		pMem = new TElem[memSize];
	}

}

TDataRoot::~TDataRoot() {

	delete[] pMem;

}

void TDataRoot::setMem(void* p, int Size) {

	if (memType == MEM_HOLDER) {
		delete[] pMem;
	}

	memType = MEM_RENTER;
	pMem = (PTElem)p;
	memSize = Size;

}

bool TDataRoot::isEmpty(void) const {

	return dataCount == 0;

}

bool TDataRoot::isFull(void) const {

	return dataCount == memSize;

}

#endif
```

### Класс TStack, заголовок и реализация
```C++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include <iostream>
#include "TDataRoot.h"

class TStack: public TDataRoot {
protected:
	int highIndex;
	int getNextIndex(int index);

public:
	TStack(int size = DefMemSize);
	virtual void put(const TData&) override;
	virtual TData get() override;
	virtual void print() override;
};

#endif
```

```C++
#ifndef __TSTACK_CPP__
#define __TSTACK_CPP__

#include <iostream>
#include "TStack.h"

TStack::TStack(int size) : TDataRoot(size), highIndex(-1) {

	if (size < 0) {
		throw 4;
	}

};


int TStack::getNextIndex(int index) {
	return ++highIndex;
}

void TStack::put(const TData& val) {

	if (pMem == nullptr) {
		throw SetRetCode(DataNoMem);
	} else if (isFull()) {
		throw SetRetCode(DataFull);
	} else {
		// Используем функцию, а не просто ++highIndex, чтобы не переписывать метод put в классах-потомках
		highIndex = getNextIndex(highIndex);
		pMem[highIndex] = val;
		dataCount++;
	}

}

TData TStack::get() {

	if (pMem == nullptr) {
		throw SetRetCode(DataNoMem);
	} else if (isEmpty()) {
		throw SetRetCode(DataEmpty);
	} else {
		dataCount--;
		return pMem[highIndex--];
	}

}

void TStack::print() {

	for (int i = 0; i < dataCount; i++) {
		std::cout << pMem[i] << " ";
	}
	std::cout << std::endl;

}
#endif
```

Очередь - всего лишь немного изменённый стек, поэтому класс TQueue с очередью будет наследовать от TStack. Сама очередь реализована в виде кольцевого буфера.

### Класс TQueue, заголовок и реализация
```C++
#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

class TQueue: public TStack {
protected:
	int lowIndex;
	int getNextIndex(int index);

public:
	TQueue(int size = DefMemSize);
	virtual TData get() override;
	virtual void print() override;
};

#endif
```

```C++
#ifndef __TQUEUE_CPP__
#define __TQUEUE_CPP__

#include <iostream>
#include "TQueue.h"'

TQueue::TQueue(int size) : TStack(size), lowIndex(0) {};

int TQueue::getNextIndex(int index) {
	return ++index % memSize;
}

TData TQueue::get() {

	if (pMem == nullptr) {
		throw SetRetCode(DataNoMem);
	} else if (isEmpty()) {
		throw SetRetCode(DataEmpty);
	} else {
		int oldIndex = lowIndex;
		lowIndex = getNextIndex(lowIndex);
		dataCount--;
		return pMem[oldIndex];
	}

}

void TQueue::print() {

	for (int i = 0, j = lowIndex; i < dataCount; i = getNextIndex(j)) {
		std::cout << pMem[j] << " ";
	}
	std::cout << std::endl;

}

#endif
```

### Тесты Google Test для класса TQueue
```C++
#include "gtest/gtest.h"
#include "TQueue.h"

// Creation
TEST(Queue, can_create_queue) {
	ASSERT_NO_THROW(TQueue queue);
}

TEST(Queue, can_create_queue_with_specified_length) {
	ASSERT_NO_THROW(TQueue queue(5));
}

TEST(Queue, cant_create_queue_with_negative_size) {
	ASSERT_ANY_THROW(TQueue queue(-5));
}

// Put/get
TEST(Queue, can_put_elem_in_queue_without_specifying_its_size) {
	TQueue queue;

	ASSERT_NO_THROW(queue.put(10));
}

TEST(Queue, can_put_elem_in_queue_with_specified_size) {
	TQueue queue(2);

	ASSERT_NO_THROW(queue.put(10));
}

TEST(Queue, can_get_elem) {
	TQueue queue(2);
	queue.put(7);

	ASSERT_NO_THROW(queue.get());
}

TEST(Queue, put_element_is_correct) {
	TQueue queue(2);
	queue.put(6);

	EXPECT_EQ(queue.get(), 6);
}

// Empty/full
TEST(Queue, empty_queue_is_empty) {
	TQueue queue(2);

	EXPECT_EQ(queue.isEmpty(), true);
}

TEST(Queue, not_full_queue_is_not_full) {
	TQueue queue(2);
	queue.put(3);

	EXPECT_EQ(queue.isFull(), false);
}

TEST(Queue, not_empty_queue_is_not_empty) {
	TQueue queue(2);
	queue.put(3);

	EXPECT_EQ(queue.isEmpty(), false);
}

TEST(Queue, full_queue_is_full) {
	TQueue queue(2);
	queue.put(3);
	queue.put(4);

	EXPECT_EQ(queue.isFull(), true);
}

// Other
TEST(Queue, cant_put_elem_when_queue_is_full) {
	TQueue queue(2);
	queue.put(3);
	queue.put(4);

	ASSERT_ANY_THROW(queue.put(5));
}
```

![](tests.png)

## Вычислительная система
Принципиально, вычислительная система построена на двух классах: "процессоре" TProc и потоке заданий TJobStream.

Второй класс агрегирует классы процесса и очереди: в очереди хранится список задач, а процессор используется для их выполнения.

В качестве дополнительного задания была предложена реализация указания длительности текущего задания (в тактах). Эта функциональность реализована и построена на специальном флаге JOB_DURATION_DISABLED: если данный флаг активен, то длительность задания будет вычисляться случайным образом.

### Класс TProc, заголовок и реализация
```C++
#ifndef __TPROC_H__
#define __TPROC_H__

// Просто флаг, обозначающий, что длительность такта для текущего задания
// будет определяться рандомно (исходя из q2). НЕ должно быть равно нулю
#define JOB_DURATION_DISABLED -1
#define MAX_CPU_PERFORMANCE 100

class TProc {
protected:
	bool isWorkingStatus;
	char q2; // "производительность" процессора, принадлежит [0; MAX_CPU_PERFORMANCE]
	int jobDuration; // длительность текущего задания в тактах

public:
	TProc(char performance);
	void cycle();
	void startNewJob(int duration = JOB_DURATION_DISABLED);
	bool isWorking();
};

#endif
```

```C++
#ifndef __TPROC_CPP__
#define __TPROC_CPP__

#include <cstdlib>
#include "TProc.h"

TProc::TProc(char performance) : q2(performance), jobDuration(JOB_DURATION_DISABLED), isWorkingStatus(false) {

	if (performance < 0 || performance > MAX_CPU_PERFORMANCE) {
		throw 1;
	}

}
void TProc::cycle() {

	if (jobDuration == JOB_DURATION_DISABLED && (rand() % MAX_CPU_PERFORMANCE < q2)
		|| !--jobDuration) {
		isWorkingStatus = false;
	}

}

void TProc::startNewJob(int duration) {

	if (duration < 0 && duration != JOB_DURATION_DISABLED) {
		throw 2;
	}

	isWorkingStatus = true;

}

bool TProc::isWorking() {

	return isWorkingStatus;

}

#endif
```

### Класс TJobStream, заголовок и реализация
```C++
#ifndef __TJOBSTREAM_H__
#define __TJOBSTREAM_H__

#include "TQueue.h"
#include "TProc.h"
#define MAX_FLOW_INTENSITY 100
#define DEFAULT_CPU_CYCLES 1000000

class TJobStream {
private:
	TProc CPU;
	TQueue jobsQueue;
	int q1; // "интенсивность потока заданий", принадлежит [0; MAX_FLOW_INTENSITY]
	int currID; // порядковый номер задания
	int stats_missed, stats_cycles, stats_useless_cycles; // статистическая информация
	void cycle();

public:
	// У jobsIntensity и performance по умолчанию макс. значение 100 
	TJobStream(char jobsIntensity, char performance, int queueLen);
	void startJobStream(int cycles = DEFAULT_CPU_CYCLES);
	void showStats();
};

#endif
```

```C++
#ifndef __TJOBSTREAM_CPP__
#define __TJOBSTREAM_CPP__

#include <cstdlib>
#include <iostream>
#include "TJobStream.h"

TJobStream::TJobStream(char jobsIntensity, char performance, int queueLen):
	q1(jobsIntensity), CPU(performance), jobsQueue(queueLen) {

	if (jobsIntensity < 0 || jobsIntensity > MAX_FLOW_INTENSITY) {
		throw 1;
	}

	currID = stats_missed = stats_cycles = stats_useless_cycles = 0;

}

void TJobStream::cycle() {

	if (CPU.isWorking()) {
		++stats_cycles;
	} else {
		++stats_useless_cycles;
	}

	if (!CPU.isWorking() && !jobsQueue.isEmpty()) {
		jobsQueue.get();
		CPU.startNewJob();
	}
	
	CPU.cycle();

}

void TJobStream::startJobStream(int cycles) {
	
	for (int i = 0; i < cycles; i++) {

		if (rand() % MAX_FLOW_INTENSITY < q1) {
			++currID;

			if (jobsQueue.isFull()) {
				++stats_missed;
			} else {
				jobsQueue.put(currID);
			}
		}

		cycle();

	}

	while (!jobsQueue.isEmpty()) {
		cycle();
	}

}

void TJobStream::showStats() {

	std::cout << "Cycles amount: " << (stats_cycles + stats_useless_cycles) << std::endl;
	std::cout << "Useless cycles: " << stats_useless_cycles << " (" <<
		(double)stats_useless_cycles / (double)(stats_cycles + stats_useless_cycles) * 100 << "%)" << std::endl;
	std::cout << "Jobs: " << currID << std::endl;
	std::cout << "Missed jobs: " << stats_missed << " (" <<
		(double)stats_missed / (double)(currID) * 100 << "%)" << std::endl;
	std::cout << "Cycles per job: " << (double)stats_cycles / (double)(currID - stats_missed) << std::endl;

}


#endif
```