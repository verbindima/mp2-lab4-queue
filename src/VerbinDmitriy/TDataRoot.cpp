#ifndef __TDATAROOT_CPP__
#define __TDATAROOT_CPP__

#include "TDataRoot.h"

TDataRoot::TDataRoot(int size): TDataCom() {

    memSize = size;
    dataCount = 0;

    if (size == 0) {
        memType = MEM_RENTER;
    } else {
        memType = MEM_HOLDER;
        pMem = new TElem[memSize];
    }

}

TDataRoot::~TDataRoot() {

    delete[] pMem;

}

void TDataRoot::setMem(void* p, int Size) {

    if (memType == MEM_HOLDER) {
        delete[] pMem;
    }

    memType = MEM_RENTER;
    pMem = (PTElem)p;
    memSize = Size;

}

bool TDataRoot::isEmpty(void) const {

    return dataCount == 0;

}

bool TDataRoot::isFull(void) const {

    return dataCount == memSize;

}

#endif