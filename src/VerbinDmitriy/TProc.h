#ifndef __TPROC_H__
#define __TPROC_H__

// ������ ����, ������������, ��� ������������ ����� ��� �������� �������
// ����� ������������ �������� (������ �� q2). �� ������ ���� ����� ����
#define JOB_DURATION_DISABLED -1
#define MAX_CPU_PERFORMANCE 100

class TProc {
protected:
    bool isWorkingStatus;
    char q2; // "������������������" ����������, ����������� [0; MAX_CPU_PERFORMANCE]
    int jobDuration; // ������������ �������� ������� � ������

public:
    TProc(char performance);
    void cycle();
    void startNewJob(int duration = JOB_DURATION_DISABLED);
    bool isWorking();
};

#endif