#ifndef __TQUEUE_CPP__
#define __TQUEUE_CPP__

#include <iostream>
#include "TQueue.h"'

TQueue::TQueue(int size) : TStack(size), lowIndex(0) {};

int TQueue::getNextIndex(int index) {
    return ++index % memSize;
}

TData TQueue::get() {

    if (pMem == nullptr) {
        throw SetRetCode(DataNoMem);
    } else if (isEmpty()) {
        throw SetRetCode(DataEmpty);
    } else {
        int oldIndex = lowIndex;
        lowIndex = getNextIndex(lowIndex);
        dataCount--;
        return pMem[oldIndex];
    }

}

void TQueue::print() {

    for (int i = 0, j = lowIndex; i < dataCount; i = getNextIndex(j)) {
        std::cout << pMem[j] << " ";
    }
    std::cout << std::endl;

}

#endif