#ifndef __TSTACK_CPP__
#define __TSTACK_CPP__

#include <iostream>
#include "TStack.h"

TStack::TStack(int size) : TDataRoot(size), highIndex(-1) {

    if (size < 0) {
        throw 4;
    }

};


int TStack::getNextIndex(int index) {
    return ++highIndex;
}

void TStack::put(const TData& val) {

    if (pMem == nullptr) {
        throw SetRetCode(DataNoMem);
    } else if (isFull()) {
        throw SetRetCode(DataFull);
    } else {
        // ���������� �������, � �� ������ ++highIndex, ����� �� ������������ ����� put � �������-��������
        highIndex = getNextIndex(highIndex);
        pMem[highIndex] = val;
        dataCount++;
    }

}

TData TStack::get() {

    if (pMem == nullptr) {
        throw SetRetCode(DataNoMem);
    } else if (isEmpty()) {
        throw SetRetCode(DataEmpty);
    } else {
        dataCount--;
        return pMem[highIndex--];
    }

}

void TStack::print() {

    for (int i = 0; i < dataCount; i++) {
        std::cout << pMem[i] << " ";
    }
    std::cout << std::endl;

}
#endif